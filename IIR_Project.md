# Projet IIR

# Détection des artéfacts "d'aliasing" dans une séquence d'images en utilisant un reseau neuronal

## I. Personnes

- Morel Adrien adrien.morel2.etu@univ-lille.fr

## II. Premier acrticle

**Anjul Patney and Aaron Lefohn. 2018. Detecting aliasing artifacts in image sequences using deep neural networks. In Proceedings of the Conference on High-Performance Graphics (HPG '18). Association for Computing Machinery, New York, NY, USA, Article 4, 1–4. DOI:https://doi.org/10.1145/3231578.3231580**

**Résumé:**

- Travaux précedants sur le sujet
  - Les métriques les plus probantes utilisables dans l'analyse d'images sont les norme
  L1 & l2 ainsi que le PSNR (Peak Signal-toNoise Ratio) qui comparent l'image a une 
  'vérité établie'.
  - D'un autre coté nous avons des métriques basées sur la perception qui fonctionnent en comparant deux images et qui
  par conséquent permettent d'identifier des artéfacts perceptibles par l'utilisateur. 
  Deux des métriques perceptives les plus populaires sont SSIM et HDR-VDP-2.
  - Ces deux méthodes nécessitent de générer une 'vérité de base' qui peut être complqué 
  voir impossible dans certains cas.
- Méthodologie:
  - L'architechture du reseau est basée sur VGG19 avec quelques modifications:
     - Utilisation de 64 x 64 x 4 séquences d'images RGB comme entrée.
     - Additions de couches hautes resolution.
     - Augmentation du nombre de fonctionalités en fonction des données expérimentales.
     - La sortie attendue est bianire : 1 pour l'ancien et 0 pour le suivant.
  - Entrainement:
     - L'entrainement du reseau neuronal est fait en founissant des images 'aliasées' 
     ainsi que 'anti-aliasée'
     - Les scènes utilisées pour le dataset sont tirées de ORCA (NVIDIA 2017).
     - Pedant l'entrainement les séquences sont 'cropées' vers un modèles 64 x 64 pour 
     correspondre a l'entrée du réseau.
     - Pour être sûr de ne pas définir comme 'aliasée' une image qui ne l'est pas (des 
     grandes zones 
     de texture identique tel que le ciel ou la mer) on compare celle-ci à sont image 
     anti-aliasée et on 
     s'assures qu'elles sont assez différentes.
  - Interférences:
     - Pendant l'interférence on procède a une évaluation par le réseau pour toutes les 64 
     x 64 'dalles'
     pour quattre images consécutives d'une séquence de test. La sortie indique la 
     magnitude de l'alisaing 
     détecté a travers l'image. Ceci permet de détecter les zones aliasées de l'image.
- Résultats:

[![Alternate Text](https://img.youtube.com/vi/zYaB827kHyc/0.jpg)](https://youtu.be/zYaB827kHyc)

## Article 2

**Filtering approaches for real-time anti-aliasing https://dl.acm.org/doi/10.1145/2037636.2037642**

L’anti-aliasing supersample (SSAA) et l’anti-aliasing multisample (MSAA) sont les solutions anti-aliasing de référence dans les jeux. Cependant, ces techniques ne sont pas bien adaptées à l’ombrage différé ou aux environnements fixes comme la génération actuelle de consoles. Dans cette étude il est expliqué comment les données des sous-pixels peuvent être utilisées pour améliorer les compromis de qualité et de performance aux étapes de post-traitement, qui est un domaine de recherche de pointe aujourd’hui. Plusieurs techniques sont mises en ouvre tel que la rechrche de filtres permettant la localisation de zones d'alisaing ainsi que leurs applications sur des consoles de jeux.

## Article 3

**Video quality assessment for computer graphics applications https://dl.acm.org/doi/10.1145/1866158.1866187**

L'étude réalisée vise a valider la performance prédictive de la nouvelle métrique introduite dans celle-ci. La capacité de la mesure à travailler sur une vidéo avec différentes plages dynamiques, le résultat sous la
forme de cartes de distorsion contenant des informations spatiales. Tout cela a demandé
la création d’un nouvel ensemble de données, car la qualité actuelle des vidéos publiques
est limitée.
La donées 'résultat' est un nombre unique indiquant la qualité globale sans aucune information sur la distribution spatiale des distorsions visibles. À cette fin, un
jeu de test de 9 paires vidéo de test de référence (1 LDR-LDR, 2 HDR-LDR,
et 6 HDR-HDR) ont été générés par l’ajout d’artefacts variant dans le temps et l’espace (comme le bruit aléatoire, la compression, la tonalité , la cartographie et la modulation de lumineusité) à 6 scènes HDR différentes. Les participants à l’étude étaient 16 sujets âgés de 23 à 50 ans, tous avec une vision presque parfaite ou corrigée. Ils ont été montrés toutes les vidéos paires côte à côte sur l’écran HDR, et ont été invités à marquer
les différences visibles (perte de détail et amplification pour les stimulis HDR-LDR ) sur une grille 16 16 affichée sur la vidéo à l’aide d’une interface utilisateur graphique.

## Article 4 

**HDR-VDP-2: a calibrated visual metric for visibility and quality predictions in all luminance conditions https://dl.acm.org/doi/10.1145/2010324.1964935**

L’objectif principal de cette étude est de créer un modèle visuel calibré pour les scènes
plage de luminance arbitraire. La manipulation d’une large gamme de luminance
est essentiel pour les nouvelles technologies d’affichage hautement dynamique ou
techniques de rendu physique, où la gamme de luminance peut
varier considérablement. La majorité des modèles visuels existants sont destinés à des plages de luminance très basses,
comme disponible sur un écran CRT ou d’impression. Plusieurs
modèles ont été proposés pour les images avec une dynamique arbitraire. Cependant,
jusqu’à présent ils n’ont pas été rigoureusement testés et calibrés par rapport aux données expérimentales. Le modèle visuel dérivé dans ce travail est le
résultat de la mise à l’essai de plusieurs autres composants du modèle
ensemble de mesures psychophysiques, en choisissant les meilleurs composants, puis en adaptant les paramètres du modèle à ces données. Il
désignera la nouvelle mesure proposée comme HDR-VDP-2.
il partage les origines et la capacité HDR avec le HDR-VDP d’origine. Cependant, la nouvelle métrique
et ses composants constituent une révision complète plutôt que
un changement progressif par rapport au HDR-VDP.

## Article 5

**Image quality assessment: from error visibility to structural similarity https://dl.acm.org/doi/10.1109/TIP.2003.819861**

Les images numériques sont sujettes à une grande variété de distorsions pendant l’acquisition, le traitement, la compression, le stockage,
transmission et reproduction, dont l’une ou l’autre peut résulter
dégradation de la qualité visuelle. Pour les images que les êtres humains doivent finalement visualiser, la seule méthode « correcte » de quantification de la qualité de l’image visuelle est l’évaluation subjective (c'est moche, flou etc ou pas?). Dans la pratique,
cependant, l’évaluation subjective est généralement pas automatisable(un humain est nécessaire).
Le but de la recherche en matière d’évaluation objective de la qualité de l’image est de
trouver une métrique permettant de prédire automatiquement la qualité de l’image perçue.

## III. Grille d'analyse

|        | Support type | Interaction type | XP: Simulation, Real... | activity type |
| ------ | ------ | ------ | ------ | ------ |
|Article 1| ... | Vidéo/Photos | Sim/réel | Gaming/Rendering |
|Article 2| ... | Vidéo/Photos | Sim | Gaming |
|Article 3| ... | Vidéo/Photos | Sim/réel | Calculs graphiques |
|Article 4| ... | Vidéo/Photos | Sim/réel | Photographie |
|Article 5| ... | Vidéo/Photos | Sim/réel | Photographie/Analyse d'images cg |

## IV. Justification

En plein boom du jeu vidéo, des images de synthèse ou encore de la réalité virtuel, avoir une image générée de grande qualité représente un défi de part le calcul de celle-ci ainsi que de l'optimisation des programmes pour une fluidité acrue.
Ces études ayant pour but de déceler des defaults dans les image traitées, de juger de la qualité d'une image de façon automatisée ainsi que de proposer des versions corrigées de ces images sont les bases sur lequelles les dévelopeurs de demains devront s'impreigner pour parvenir a fournir des programmes de qulaité et faire avancer les capacités dans ces domaines. Les applications de ces études sont déja nécessaires a ce jour ce qui en fait un sujet extremement pertinent.


